[![Code style: black](https://img.shields.io/badge/Python%20-Code-0000.svg?&style=flat-square&logo=python&color=purple)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Pandas%20-library-0000.svg?&style=flat&logo=pandas&color=orange)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Visual_Studio_Code%20-IDE-0000.svg?style=palstic&logo=Visual-Studio-Code&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Numpy%20-library-0000.svg?style=palstic&logo=numpy&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Style%20-Plastic-0000.svg?style=palstic&logo=appveyor&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/TensorFlow%20-module-0000.svg?style=palstic&logo=TensorFlow&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Google_Colab%20-notebook-0000.svg?style=palstic&logo=Google-Colab&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Jira%20-ticket-0000.svg?style=palstic&logo=jira&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Google_Cloud%20-paltform-0000.svg?style=palstic&logo=Google-Cloud&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Power_BI%20-analytics-0000.svg?style=palstic&logo=Power-BI&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/SAP%20-database-0000.svg?style=palstic&logo=SAP&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Anaconda%20-package-0000.svg?style=palstic&logo=Anaconda&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Poetry%20-packaging-0000.svg?style=palstic&logo=Poetry&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Dotenv%20-packaging-0000.svg?style=palstic&logo=.env&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/DBT%20-workflow-0000.svg?style=palstic&logo=dbt&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Jinja%20-templating-0000.svg?style=palstic&logo=jinja&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/SQLite%20-library-0000.svg?style=palstic&logo=SQlite&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Confluence%20-Doc-0000.svg?style=palstic&logo=Confluence&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Atom%20-IDE-0000.svg?style=palstic&logo=atom&color=blue)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Postman%20-RESTful_API-0000.svg?style=palstic&logo=postman&color=blue)](https://code.visualstudio.com/)


Welcome to your new dbt project!

### Using the starter project

Try running the following commands:
- dbt run
- dbt test


### Resources:
- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](https://community.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices

<h1>Generate an SSH key pair</h1>

<table>
  <tr>
    <th>Algorithm</th>
    <th>Public key</th>
    <th>Private key</th>
  </tr>
  <tr>
    <td>ED25519 (preferred)</td>
    <td>id_ed25519.pub</td>
    <td>id_ed25519</td>
  </tr>
  <tr>
    <td>RSA (at least 2048-bit key size)</td>
    <td>id_rsa.pub</td>
    <td>id_rsa</td>
  </tr>
</table>

If you do not have an existing SSH key pair, generate a new one:
Open a terminal.
Run `ssh-keygen -t` followed by the key type and an optional comment. This comment is included in the `.pub` file that’s created. You may want to use an email address for the comment.
```bash
For example, for ED25519:
ssh-keygen -t ed25519 -C "<comment>"
```
For 2048-bit RSA:
```bash
ssh-keygen -t rsa -b 2048 -C "<comment>"
```
Press Enter. Output similar to the following is displayed:
```bash
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
```
Accept the suggested filename and directory, unless you are generating a deploy key or want to save in a specific directory where you store other keys.
You can also dedicate the SSH key pair to a specific host.
Specify a passphrase:
```bash
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

A confirmation is displayed, including information about where your files are stored.

<h1>Add an SSH key to your GitLab account</h1>

To use SSH with GitLab, copy your public key to your GitLab account:
<ol>
<li>Copy the contents of your public key file. You can do this manually or use a script. For example, to copy an RSA key to the clipboard:</li>
macOS:

```bash
tr -d '\n' < ~/.ssh/id_rsa.pub | pbcopy
```
Linux (requires the xclip package):

```bash
xclip -sel clip < ~/.ssh/id_rsa.pub
```
Git Bash on Windows:

```bash
cat ~/.ssh/id_rsa.pub | clip
```
<li>Sign in to GitLab.</li>
<li>On the top bar, in the top right corner, select your avatar.</li>
<li>Select Preferences.</li>
<li>On the left sidebar, select SSH Keys.</li>
<li>In the Key box, paste the contents of your public key.</li>
<li>In the Title box, type a description, like Work Laptop or Home Workstation.</li>
<li>Optional. Select the Usage type of the key. It can be used either for Authentication or Signing or both. Authentication & Signing is the default value.</li>
<li>Optional. Update Expiration date to modify the default expiration date.</li>
<li>Select Add key.</li>
</ol>




<h1>Unix/Linux Command Tips:</h1>

Create directory:

<code>>mkdir {foldername}</code>

Move file from home directory to another directory:

<code>>mv ~/{file name} ~/{foldername}</code>

Move multiple files:

<code>>mv {file name1} {file name2} {file name3} ~/{foldername}</code>

Move files Verbose option

<code>mv ~/*.{file ext.} ~/{folder name}</code>

Remove a folder with its contents:

<code>>rmdir /s {foldername}</code>

Delete a file:

<code>>del {filename}</code>

Print a file tree:

<code>>tree /f</code>

For tilde On a Windows PC: Enable Num Lock, press and hold Alt GR, then type the character's specific key

<h1 style= "color: green; text-align: left">Setting up Environments and Programs:</h1>
<ol>
<li>Install python in <code>C:\Program Files (x86)\{Python-Folder}\{Python39-Folder}\</code></li>

<img src=img/img1.png>
<img src=img/img4.png>
<img src=img/img5.png>
<img src=img/img6.png>
<img src=img/img7.png>
<img src=img/img8.png>

<li>Install Visual Studio Code in local user folder <code>C:\Users\{user}></code></li>

<img src=img/img9.png>

</ol>
<h2>Visual Studio Code Setup:</h2>

<ul>
  <li> Copy and paste this <code>%APPDATA%\Code\User\</code> on <mark style="background: orange">File Explorer</mark> and press <mark style="background: orange">Enter</mark></li>
  <li>Paste the following dict into the <code>settings.json</code> file</li>
</ul>

<img src=img/img2.jpg>

<img src=img/img3.jpg>

```json
{
  // opens json file instead of settings UI
  "workbench.settings.editor": "json",
  "python.pythonPath": "${workspaceFolder}/.venv/bin/python",
  "python.envFile": "${workspaceFolder}/.venv",
  "terminal.integrated.env.osx": {
    "DBT_ENV_GUJ_UTILS_DEPLOY_USERNAME": "<replace-me>",
    "DBT_ENV_SECRET_GUJ_UTILS_DEPLOY_TOKEN": "<replace-me>"
  },
  "terminal.integrated.env.windows": {
    "DBT_ENV_GUJ_UTILS_DEPLOY_USERNAME": "<replace-me>",
    "DBT_ENV_SECRET_GUJ_UTILS_DEPLOY_TOKEN": "<replace-me>"
  },
  "terminal.integrated.env.linux": {
    "DBT_ENV_GUJ_UTILS_DEPLOY_USERNAME": "<replace-me>",
    "DBT_ENV_SECRET_GUJ_UTILS_DEPLOY_TOKEN": "<replace-me>"
  },
    "editor.indentSize": "tabSize",
    "breadcrumbs.enabled": false,
    "editor.wordWrap": "on",
    "files.autoSave": "afterDelay",
    "files.insertFinalNewline": true,
    "files.trimTrailingWhitespace": true,
    "editor.formatOnPaste": true,
    "editor.tabSize": 2,
    "editor.rulers": [
        120
    ],
    "editor.renderWhitespace": "all",
    "editor.linkedEditing": true,
    "editor.minimap.enabled": false,
    // You might want to see the file you currently have open in your file explorer
    "explorer.autoReveal": true,
    "explorer.confirmDelete": false,
    "explorer.confirmDragAndDrop": false,
    "explorer.compactFolders": false,
    "git.confirmSync": false,
    "git.autofetch": true,
    "diffEditor.ignoreTrimWhitespace": false,
    "markdown.preview.scrollEditorWithPreview": true,
    "markdown.preview.scrollPreviewWithEditor": true,
    "terminal.integrated.defaultProfile.osx": "zsh",
    "terminal.integrated.copyOnSelection": true,
    "terminal.integrated.cursorBlinking": true,
    "redhat.telemetry.enabled": false,
    "window.title": "${activeEditorLong}${separator}${rootName}",
    "workbench.colorTheme": "Visual Studio Dark",
    "workbench.editor.showTabs": true,
    "workbench.editor.showIcons": true,
    "workbench.iconTheme": "vs-seti",
    "[jinja-sql]": {
        "editor.defaultFormatter": "dorzey.vscode-sqlfluff"
    }
}
```
3. Install [gcloud CLI](https://cloud.google.com/sdk/docs/install) in <code style= "background: Burlywood; color: blue">windows>Program Files(x86)</code>
4. Install [Git SCM to Windows](https://gitforwindows.org/)  locally at user level <code style= "background: Burlywood; color: blue">C:\Users\\{user}\\</code>
<img src=img/img10.jpg>
5. Install [Poetry](https://python-poetry.org/docs/) locally at user level <code style= "background: Burlywood; color: blue">C:\Users\\{user}></code>

<ul>
  <li>Linux, macOS, Windows (WSL):</li>
  <code>>curl -sSL https://install.python-poetry.org | python3 -</code>

  <li>Homebrew:</li>
  If homebrew is not installed in your system follow the following steps:
  <code>>/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"</code>

  create the `.zshrc` file if it does not exist with the following command:

  <code>>touch ~/.zshrc</code> or

  <code>>echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile</code> or

  <code>>echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zshrc</code>

  Open the `.zshrc` file

  <code>>open ~/.zshrc</code>

  Add the following line of code at the end of `.zshrc` file

  <code>export PATH=/opt/homebrew/bin:$PATH</code>

  Make the changes available by the following command:

  <code>>source ~/.zshrc</code>

  Check if everything works fine:

  <code>>brew help</code>

  To install Visual Studio Code:

  <code>>brew install visual-studio-code</code>

  <code>>[brew install poetry](https://formulae.brew.sh/formula/poetry)</code>

  <li>Windows (Powershell):</li>
  <code>>(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -</code> or

  <code>>[pip install poetry](https://pypi.org/project/poetry/)</code>

</ul>

<h1 style='color: green; text-align: left'>Creating New project:</h1>

```d
>poetry new --src <project name>
>cd <project name>
>code .
>poetry config --list

cache-dir = "C:\\Users\\<alias>\\AppData\\Local\\pypoetry\\Cache"
experimental.new-installer = true
experimental.system-git-client = false
installer.max-workers = null
installer.no-binary = null
installer.parallel = true
repositories.guj_packages.url = "https://git.zd.guj.de/api/v4/projects/984/packages/pypi/<folder name>"
virtualenvs.create = true
virtualenvs.in-project = true
virtualenvs.options.always-copy = false
virtualenvs.options.no-pip = false
virtualenvs.options.no-setuptools = false
virtualenvs.options.system-site-packages = false
virtualenvs.path = "{cache-dir}\\virtualenvs"  # C:\Users\<alias>\AppData\Local\pypoetry\Cache\virtualenvs
virtualenvs.prefer-active-python = false
virtualenvs.prompt = "{project_name}-py{python_version}"
```

If <code>virtualenvs.in-project = null</code>, change it into <code>true</code> with the following command:
```C++
>poetry config virtualenvs.in-project true
>poetry install
>.\.venv\Scripts\activate
>poetry add dbt-bigquery
```
<h2>Note:</h2>

If you already created a project you can use:

<code>>poetry init</code> to generate the `pyproject.toml` file

---

If the command <code>poetry add dbt-bigquery</code> throws an error related to your python version do the following steps:

To check the path of your python: <code>>which python<x.y></code> on Mac OS and <code>>where python</code> on Windows

To change python executable run: <code>>poetry env use /usr/local/bin/python<x.y></code>

---

```C++
>dbt init
  08:47:29  Running with dbt=1.3.2
  Enter a name for your project (letters, digits, underscore): {your project name}
  Which database would you like to use?
  [1] bigquery
  [2] <adapter>
  [3] <adapter>

  (Don't see the one you want? https://docs.getdbt.com/docs/available-adapters)

  Enter a number: 1
  [1] oauth
  [2] service_account
  Desired authentication method option (enter a number): 2
  keyfile (/path/to/bigquery/keyfile.json): {path to your json file}
  project (GCP project id): {your gcp project ID}
  dataset (the name of your dbt dataset): {name of your dataset}
  threads (1 or more): 4
  job_execution_timeout_seconds [300]: 300
  [1] US
  [2] EU
  Desired location option (enter a number): 2
  08:50:48  Profile {project name} written to C:\Users\{alias}\.dbt\profiles.yml using target's profile_template.yml and your supplied values. Run 'dbt debug' to validate the connection.

  For more information on how to configure the profiles.yml file,
  please consult the dbt documentation here:

    https://docs.getdbt.com/docs/configure-your-profile

  One more thing:

  Need help? Don't hesitate to reach out to us via GitHub issues or on Slack:

    https://community.getdbt.com/

  Happy modeling!
```
```C++
>dbt debug
>dbt run -m <model name>
```

<h1 ><b><mark style= "background: green; color: blue">Creating External Tables in BigQuery</mark></b></h1>

Firs run the following command to create the external table in bigquery:

<code>>dbt run-operation stage_external_sources  --vars 'ext_full_refresh: true' --args "select: src__employee.dz_employee_data_ext"</code>

<h2><mark style= "background:green; color: black">Some useful poetry commands</mark></h2>

To add libraries and dependencies to your pyproject.toml file:

<code>>poetry add pandas</code>

To remove dependencies and libraries from your pyproject.toml file:

<code>>poetry remove pandas</code>

To check where the .venv file is located:

<code>>poetry env list

dbt-bi-officeservice-Xkz2JfGC-py3.9 (Activated)</code>

To remove venv:

<code>>poetry env remove dbt-bi-officeservice-Xkz2JfGC-py3.9</code>

To re-setup virtual environment into your project:

<code>>poetry install</code> or <code>poetry init</code>

To deactivate venv:

<code>(.venv) ...>deactivate</code>


<h1 style='color: green; text-align: left'>Setting up google cloud:</h1>

<code>>gcloud init</code>

<code>>gcloud auth application-default login</code>

<code>>gcloud config get-value project</code>

<code>>gcloud config set project ---> what is this?</code>

<code>C:\Users\\{user}>gcloud auth login example@guj.cloud</code>


gcloud config set project gcp-eis-bi-hr-dev


```C++
Welcome to the Google Cloud CLI! Run "gcloud -h" to get the list of available commands.
---
Welcome! This command will take you through the configuration of gcloud.

Your current configuration has been set to: [default]

You can skip diagnostics next time by using the following flag:
  gcloud init --skip-diagnostics

Network diagnostic detects and fixes local network connection issues.
Checking network connection...done.
Reachability Check passed.
Network diagnostic passed (1/1 checks passed).

You must log in to continue. Would you like to log in (Y/n)?  Y

Your browser has been opened to visit:

    https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=32555940559.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A8085%2F&scope=openid+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcloud-platform+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fappengine.admin+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fsqlservice.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcompute+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Faccounts.reauth&state=Hjompo5dORu3sn0O5Mk6PgbOl0gwGb&access_type=offline&code_challenge=kSUGswRr0sjJl4jIObQKEOPwrqa6Bzy_3UdjbUjVdck&code_challenge_method=S256

You are logged in as: [exanple@guj.cloud].

Pick cloud project to use:
 [1] gcp-eis-bi-hr-dev
 [2] gcp-eis-bi-hr-prd
 [3] gcp-eis-bi-hr-qat
 [4] gcp-eis-bi-officeservice-dev
 [5] gcp-eis-bi-officeservice-prd
 [6] gcp-eis-bi-officeservice-qat
 [7] mgr-daoserving-preprod-u3wu
 [8] mgr-daoserving-prod-jwsf
 [9] Enter a project ID
 [10] Create a new project
Please enter numeric choice or text value (must exactly match list item): 4
```

####




####

### Notes:
To check your current Python version, run python3 -V or python -V
use the following command to install dbt-<adapter> package
>pip install dbt-bigquery
check the dbt version with
>dbt --version
intialize dbt with:
>dbt init <project_name>
>cd <project_name>
>python3 -m venv .dbtenv
>source .dbtenv/bin/activate
to check the profiles.yml file:
>cd ~/.dbt/
>ls (dir for windows)
>open profiles.yml
to get to the project folder:
>cd ~/google_cloud_platform/dbt_bigquery_project/

if there is a problem with python version while running poetry add dbt-bigquery run first
>which python<3.10> to get the path
>poetry env use /usr/local/bin/python3.10

# ls Options:

❯ ls -L

-1      -- single column output
-@      -- display extended attribute keys and sizes in long listing
-A      -- list all except . and ..
-B      -- print octal escapes for control characters
-C      -- list entries in columns sorted vertically
-F      -- append file type indicators
-H      -- follow symlinks on the command line
-O      -- display file flags
-P      -- do not follow symlinks
-R      -- list subdirectories recursively
-S      -- sort by size
-T      -- show complete time information
-U      -- file creation time
-W      -- display whiteouts when scanning directories
-a      -- list entries starting with .
-b      -- as -B, but use C escape codes whenever possible
-c      -- status change time
-d      -- list directory entries instead of contents
-e      -- display ACL in long listing
-f      -- output is not sorted
-g      -- long listing but without owner information
-h      -- print sizes in human readable form
-i      -- print file inode numbers
-k      -- print sizes in kilobytes
-l      -- long listing
-m      -- comma separated
-n      -- numeric uid, gid
-o      -- long listing but without group information
-p      -- append file type indicators for directory
-q      -- hide control chars
-r      -- reverse sort order
-s      -- display size of each file in blocks
-t      -- sort by modification time
-u      -- access time
-v  -w  -- print raw characters
-x      -- sort horizontally

#### Set zsh as default for macOS account

Update macOS account to use zsh as the default by running
❯ chsh -s /bin/zsh

#### Set zsh as the default for VSCode

If you use VSCode or another IDE with an integrate terminal then you may need to update it to use <code>zsh</code> as the default for the integrated terminal or else it will default to the previous default.
<ol>
<li>Open VSCode</li>
<li>Press SHIFT + COMMAND + P</li>
<li>type in "Terminal" and select "Terminal: Select Default Profile" from the dropdown</li>
<li>select zsh</li>
</ol>

<p class="css-0">
<img src="img/shell.gif" alt="gif of selecting default profile in VSCode" class="css-0">
</p>

❯ open ~/.zshrc

or

❯ vim ~/.zshrc

or

❯ code ~/.zshrc

#### uninstall python

sudo rm -rf “/Applications/Python”
sudo rm -rf /Library/Frameworks/Python.framework
sudo rm -rf /usr/local/bin/python

python_version_number=3.10
sudo rm -rf /Library/Frameworks/Python.framework/Versions/${python_version_number}/
sudo rm -rf "/Applications/Python ${python_version_number}/"
cd /usr/local/bin && ls -l | grep "/Library/Frameworks/Python.framework/Versions/${python_version_number}" | awk '{print $9}' | sudo xargs rm

Removing symbolic links

To list the broken symbolic links.

ls -l /usr/local/bin | grep '../Library/Frameworks/Python.framework/Versions/[version number]'

And to remove these links:

<code>cd /usr/local/bin</code>

<code>ls -l /usr/local/bin | grep '../Library/Frameworks/Python.framework/Versions/[version number]' | awk '{print $9}' | tr -d @ | xargs rm*</code>

####

[![Code style: black](https://img.shields.io/badge/Follow%20-1.3k-0000.svg?style=social&logo=Facebook)](https://www.facebook.com/live/producer/v2/656577382837761/dashboard)
[![Code style: black](https://img.shields.io/badge/Follow%20-2.3k-0000.svg?style=social&logo=twitter)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Follow%20-2.5k-0000.svg?style=social&logo=instagram)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Follow%20-1.5k-0000.svg?style=social&logo=linkedin)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/Follow%20-15k-0000.svg?style=social&logo=youtube)](https://www.youtube.com/)




